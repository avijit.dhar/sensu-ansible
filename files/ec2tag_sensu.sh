#!/bin/bash
## This Scrips use the IAM role creetnials to recover the instace tags, and set itas it's not support conversion to lowercase in sensu file.

function validate_json {
    jq ${jq_args} '.' ${1} > /dev/null 2>&1
    local status=$?
    if [ ${status} -ne 0 ]; then
        echo "Invalid JSON File"
	exit 1
    fi
}
function cleanup {
	for file in ${tags_file} ${sensu_temp} ${sensu_temp2} ${json_temp_file} ${json_temp_file2}; do
		[ -f ${file} ] && rm -f ${file}
	done
}
function usage {
	echo -e "
	$(basename $0) [--keyfile \033[4mkey_file\033[0m] [--access_key \"ACCESS_KEY\"] [--secret_key \"SECRET_KEY\"] [--proxy http://xxxx:8080] [--no_hostnname]]
AWS Credential can be in : 1) IAM ROLE , 2) key_file 3) access_key & secret_key\n
    - \033[1mkeyfile\033[0m     configuration file with a AWS_ACCESS_KEY_ID & AWS_SECRET_ACCESS_KEY
    - \033[4mACCESS_KEY\033[0m  Pass ACCESS_KEY
    - \033[4mSECRET_KEY\033[0m  Pass SECRET_KEY as argument
    - \033[4mproxy\033[0m  Specifiy the htttps proxy

	"

}
function echoerr() { cat <<< "$@" 1>&2; }

# By default: dry-run : false
DRY=0
# By default: update hostname in sensu is aws tag name exist
DISABLE_HOSTNAME=0
# Parsing Argument
while [ $# -ne 0 ]
do
        case ${1} in
        -h|-H|help|-help|-HELP)
                usage; exit
                ;;
        --keyfile|-keyfile|-k|-f)          FILE=${2};              	shift; shift;;
        --access_key|-access_key|-a)    AWS_ACCESS_KEY_ID=${2}; 	shift; shift;;
        --secret_key|-secret_key|-s)    AWS_SECRET_ACCESS_KEY=${2}; 	shift; shift;;
        --proxy|-proxy|-p)    HTTPS_PROXY=${2}; 			shift; shift;;
        --dry-run|-dry-run|-dry|-d)     DRY=1;              		shift;;
        --no_hostnname|-no_hostname)	DISABLE_HOSTNAME=1;		shift;;
        *) usage; exit ;;
        esac
done
tags_file="$(mktemp /tmp/tags.XXXXX || exit 1)"
sensu_temp="$(mktemp /tmp/senuclient.XXXXX || exit 1)"

trap 'cleanup' SIGINT SIGTERM SIGKILL EXIT INT TERM
# JQ is available ?
which jq > /dev/null 2>&1
if [[ ${?} -ne 0 ]] ; then
	# trying to found it on specific PATH
	export PATH=/oper/ccm/bin/:${PATH}
	which jq > /dev/null 2>&1
	if [[ ${?} -ne 0 ]] ; then
		echoerr "Error : unable to find JQ"
		exit 2
	fi
fi
which ec2metadata > /dev/null 2>&1
if [[ ${?} -eq 0 ]] ; then
	INSTANCE_ID=$(ec2metadata --instance-id)
else
	INSTANCE_ID=$(wget -O - -q --timeout=2 --tries=2 http://169.254.169.254/latest/meta-data/instance-id)
fi
REGION=$(wget -O - -q --timeout=2 --tries=2 http://169.254.169.254/latest/dynamic/instance-identity/document | jq -r .region)
INSTANCE_PROFILE=$(wget -q -O - --timeout=2 --tries=2 http://169.254.169.254/latest/meta-data/iam/security-credentials/)
# Try to get AwS Credential via IAM ROLE : 
if [[ -n ${INSTANCE_PROFILE} ]] ; then
	aws_cred=$(wget -q --timeout=2 --tries=2 -O - http://169.254.169.254/latest/meta-data/iam/security-credentials/${INSTANCE_PROFILE})
	# Get Credentials
	aws_access_key_id=$(jq -r .AccessKeyId <<< "${aws_cred}")
	aws_secret_access_key=$(jq -r .SecretAccessKey <<< "${aws_cred}")
	aws_session=$(jq -r .Token <<< "${aws_cred}")
	export AWS_ACCESS_KEY_ID=${aws_access_key_id}
	export AWS_SECRET_ACCESS_KEY=${aws_secret_access_key}
	export AWS_SESSION_TOKEN=${aws_session}
fi
# Trying to get AwS Credential via key_file
if [[ -f $FILE ]] ; then
	source ${FILE}
	# OLD VARIABLE name ? AWS_ACCESS_KEY & AWS_SECRET_KEY
	[[ -n ${AWS_ACCESS_KEY} && -z ${AWS_ACCESS_KEY_ID} ]] && export AWS_ACCESS_KEY_ID=${AWS_ACCESS_KEY}
	echo "xxx"
	[[ -n ${AWS_SECRET_KEY} && -z ${AWS_SECRET_ACCESS_KEY} ]] && export AWS_SECRET_ACCESS_KEY=${AWS_SECRET_KEY}

fi
# Trying to get AwS Credential via access key / secret key argument
if [[ -n ${AWS_ACCESS_KEY_ID} && -n ${AWS_SECRET_ACCESS_KEY} ]] ; then
	export AWS_ACCESS_KEY_ID=${AWS_ACCESS_KEY_ID}
	export AWS_SECRET_ACCESS_KEY=${AWS_SECRET_ACCESS_KEY}
fi

# $? on jq only work if used with -e (available since version 1.4
jq_args=''

# AWSCLI is available ?
which aws > /dev/null 2>&1
if [[ ${?} -ne 0 ]] ; then
	# trying to found it on specific PATH
	export PATH=/oper/python27/bin/:${PATH}
	which aws > /dev/null 2>&1
	if [[ ${?} -ne 0 ]] ; then
		echoerr "Error: unable to find AWS CLI"
		exit 2
	fi
fi
# Use Proxy if needed : 
[[ -n ${HTTPS_PROXY} ]] && export HTTPS_PROXY="${HTTPS_PROXY}"

tags=$(aws ec2 describe-tags --filters "Name=resource-id,Values=${INSTANCE_ID}" --region=${REGION})
if [ ${?} -ne 0 ]; then
	echoerr "Error getting tags with aws cli"
	exit 1
fi
# Testing version of JQ as explode/implode & function ascii_downcase only support on recent version
jq_version=$(jq --version 2>&1 |tr -d -c 0-9)
# Debian recompile jq & get hesoteric version :
# jq-1.4-1-e73951f => use bash tips to remove minor version e73951f
#TODO : don't use grep -v AutoScal as it's can break json file (if it's the last line)
if [[ ${jq_version:0:2} -ge 15 ]] ; then
	jq_args='--exit-status'
	RES=$(jq '.Tags | map({(.Key|tostring|ascii_downcase) : .Value}) | add | {"client": . }' <<< ${tags})
elif [[ ${jq_version:0:2} -ge 14 ]] ; then
	jq_args='--exit-status'
	# Add function to lowercase
	RES=$(jq 'def ascii_downcase:explode | map( if 65 <= . and . <= 90 then . + 32  else . end) | implode; .Tags | map({(.Key|tostring|ascii_downcase) : .Value}) | add | {"client": . }' <<< ${tags})
else
	# Make trick for jq 1.3 as it's not support conversion to lowercase:
	RES=$(jq '.Tags | map({(.Key|tostring) : .Value}) | add | {"client": . }'<<< ${tags} | sed -r 's/("[[:alnum:]]*":)(.*)/\L\1\E\2/g')
fi
#echo $tags | jq '.Tags | map({(.Key|tostring|explode | map( if 65 <= . and . <= 90 then . + 32  else . end) | implode) : .Value}) | add | {"client": . }' | grep -v AutoScal > ${tags_file}
grep -qi autoscal  <<< ${RES}
if [[ ${?} -eq 0 ]] || [[ ${DISABLE_HOSTNAME} == 1 ]] ; then
	# Old behavior delete name field
	#jq 'del(.client.name)' <<< ${RES} > ${tags_file}
	# New : Use instead curl http://169.254.169.254/latest/meta-data/hostname (with short name) when in a autoscaling
	NAME=$(curl http://169.254.169.254/latest/meta-data/hostname)
	json_temp_file="$(mktemp /tmp/senuclient.XXXXX || exit 1)"
	json_temp_file2="$(mktemp /tmp/senuclient.XXXXX || exit 1)"
	echo -e '{\n  "client": {\n    "name": "'${NAME%%.*}'"\n  }\n}' > ${json_temp_file}
	# Need to write RES to a temp file : 
	echo "${RES}" > ${json_temp_file2}
	jq ${jq_args} -s '.[0] * .[1]' ${json_temp_file2} ${json_temp_file} > ${tags_file}
else
	echo "${RES}" > ${tags_file}
fi
validate_json ${tags_file}
if [ -f /tmp/tag.json ]; then
	cmp --silent ${tags_file} /tmp/tag.json || mv ${tags_file} /tmp/tag.json
else
	mv ${tags_file} /tmp/tag.json
fi
# Only work with jq >= 1.4
if [[ ${jq_version:0:2} -eq 13 ]] ; then
	echoerr "jq version is too old, need update to concart json file"
	exit 2
fi
# Apply change only if dry-run is not set :
if [[ "${DRY}" == 0 ]] ; then
	sensu_temp2="$(mktemp /tmp/senuclient.XXXXX || exit 1)"
	jq 'del(.client.name)' /etc/sensu/conf.d/client.json > ${sensu_temp2}
	jq ${jq_args} -s '.[0] * .[1]' ${sensu_temp2} /tmp/tag.json> ${sensu_temp}
	if [ ${?} -eq 0 ] ; then
		validate_json ${sensu_temp}
		cp ${sensu_temp} /etc/sensu/conf.d/client.json
		service sensu-client restart
	else
		echoerr 'Error Validating new config file'
		exit 1
	fi
else
	validate_json ${sensu_temp}
	echo "dont make any change as dry-run is enabled"
fi
exit 0
