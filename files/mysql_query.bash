#!/bin/bash
# sensu check to make a alert
# Sensu Return Staus :
#    0: ok
#    1: warning
#    2: critical
#    3 or more: unknown

# Parsing Argument
function cleanup {
	for file in ${sensu_temp} ; do
		[ -f ${file} ] && rm -f ${file}
	done
}
function usage {
	echo -e "
	$(basename $0) --user \033[4muser\033[0m --password \033[4mpassword\033[0m [--socket \033[4m/tmp/mysqld.sock\033[0m] [--description \033[4m\"Cache hit\"\033[0m] --mysql-query /root/mysql_cache_hits.sql [--warn-over \033[4m80\033[0m] [--warn-under \033[4m20\033[0m] [--critical-over \033[4m90\033[0m] [--critical-under \033[4m10\033[0m]

	"

}

# By default: dry-run : false
DRY=0
while [ $# -ne 0 ]
do
        case ${1} in
        -h|-H|help|-help|-HELP)
                usage; exit
                ;;
        --user|-user|-u)          		USER=${2};              	shift; shift;;
        --password|-password|-p)		PASSWORD=${2};             	shift; shift;;
        --socket|-socket|-s)			SOCKET=${2};             	shift; shift;;
        --host|-host)					HOST=${2};           	  	shift; shift;;
        --description|-d)				DESCRIPTION=${2};			shift; shift;;
        --warn-over|-w)					WARNING_OVER=${2};			shift; shift;;
        --warn-under|-W)				WARNING_UNDER=${2};			shift; shift;;
        --critical-over|-w)				CRITICAL_OVER=${2};			shift; shift;;
        --critical-under|-W)			CRITICAL_UNDER=${2};		shift; shift;;
        --mysql-query|-m)				FILE=${2};					shift; shift;;
        --dry-run|-dry-run|-dry|-d)     DRY=1;              		shift;;
        *) usage; exit ;;
        esac
done
trap 'cleanup' SIGINT SIGTERM SIGKILL EXIT INT TERM

# Testing mandatory argument
if [[ -z ${USER} ]] || [[ -z ${PASSWORD} ]] || [[ -z ${FILE} ]] ; then
	echo "UNKNOWN: Argument mandatory are missing"
	usage
	exit 3	
fi
if ! [[ -f ${FILE} ]] ; then
	echo "UNKNOWN: ${FILE} is not a file"
	exit 3
fi
if [[ -z "${DESCRIPTION}" ]] ; then
	DESCRIPTION=${FILE}
fi

# --host=host_name or socket is mandatory
if [[ -n ${SOCKET} && -n ${HOST} ]] ; then
	echo "UNKNOWN: ONLY one argument of --socket and --host"
	usage
	exit 3	
fi
if [[ -n ${HOST} ]] ; then
	RES=$(mysql --silent --user=${USER} --password="${PASSWORD}" --host=${HOST} < ${FILE})
else
	RES=$(mysql --silent --user=${USER} --password="${PASSWORD}" --socket ${SOCKET:-/var/run/mysqld/mysqld.sock} < ${FILE})
fi

# Should be return a number, try to converting it to a integer

typeset -i VALUE
VALUE=${RES%.*}
if [[ -z ${RES} ]] ; then
	echo "UNKNOWN: mysql dont return value"
	exit 3
fi


#CRITICAL_OVER
if [[ -n ${CRITICAL_OVER} ]] && [[ ${VALUE} -gt ${CRITICAL_OVER} ]] ; then
	echo "CRITICAL: ${RES} is over than critical throushoot ${CRITICAL_OVER} for ${DESCRIPTION}"
	exit 2
fi
#CRITICAL_UNDER
if [[ -n ${CRITICAL_UNDER} ]] && [[ ${VALUE} -lt ${CRITICAL_UNDER} ]] ; then
	echo "CRITICAL: ${RES} is lower than critical throushoot ${CRITICAL_UNDER} for ${DESCRIPTION}"
	exit 2
fi
#WARNING_OVER
if [[ -n ${WARNING_OVER} ]] && [[ ${VALUE} -gt ${WARNING_OVER} ]] ; then
	echo "WARNING: ${RES} is over than warning throushoot ${WARNING_OVER} for ${DESCRIPTION}"
	exit 2
fi
#WARNING_UNDER
if [[ -n ${WARNING_UNDER} ]] && [[ ${VALUE} -lt ${WARNING_UNDER} ]] ; then
	echo "WARNING: ${RES} is lower than warning throushoot ${WARNING_UNDER} for ${DESCRIPTION}"
	exit 2
fi

echo "OK: ${RES} for ${DESCRIPTION}"
exit 2
