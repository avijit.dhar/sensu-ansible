#!/bin/bash
# sensu check to make a alert if too much php session
# Sensu Return Staus :
#    0: ok
#    1: warning
#    2: critical
#    3 or more: unknown

# Parsing Argument
function cleanup {
	for file in ${sensu_temp} ; do
		[ -f ${file} ] && rm -f ${file}
	done
}
function usage {
	echo -e "
	$(basename $0) [--php_session-path \033[4mdir\033[0m]

	"

}

# By default: dry-run : false
DRY=0
while [ $# -ne 0 ]
do
        case ${1} in
        -h|-H|help|-help|-HELP)
                usage; exit
                ;;
        --php_session-path|-p)          PHP_DIR=${2};              	shift; shift;;
        --critical|-c)					CRITICAL=${2};              	shift; shift;;
        --warning|-w)					WARNING=${2};              	shift; shift;;
        --dry-run|-dry-run|-dry|-d)     DRY=1;              		shift;;
        *) usage; exit ;;
        esac
done
trap 'cleanup' SIGINT SIGTERM SIGKILL EXIT INT TERM

# Testing PHP_DIR
if [[ -z ${PHP_DIR} ]] ; then
	echo "UNKNOWN: Argument --php_session-path is mandatory"
	exit 3	
fi
if ! [[ -d ${PHP_DIR} ]] ; then
	echo "UNKNOWN: ${PHP_DIR} is not a direcotry"
	exit 3
fi

nb_session=$(find ${PHP_DIR} -mmin 1 -name "sess_*" | wc -l)

#Critical default value : 100
if [[ ${nb_session} -gt ${CRITICAL:-100} ]] ; then
	echo "CRITICAL: ${nb_session} active sessions"
	exit 2
fi
#Warning default value : 50
if [[ ${nb_session} -gt ${WARNING:-50} ]] ; then
	echo "WARNING: ${nb_session} active sessions"
	exit 1
fi
echo "OK: ${nb_session} active sessions"
exit 0
