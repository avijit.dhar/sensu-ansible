# Set RabbitMQ Certificate :

The Key and Cert should be encrypted.
Use :
```
ansible-vault encrypt conf/sensu/amundi/sensu_key.pem
ansible-vault encrypt conf/sensu/amundi/sensu_cert.pem
```
# Plugins Installation
List of Plugins installed remotly is the combination of 3 list : 
 - sensu_plugins_default (shoudn't be modified)
 - sensu_plugins_group
 - sensu_plugins_host

it's permit to install specific plugin on servers
# Information about the Roles

Created from the role  https://github.com/sensu/sensu-ansible
